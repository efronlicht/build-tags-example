# build tags minimal example


build tags in go are at the FILE level.

see the [official documentation](https://golang.org/cmd/go/#hdr-Build_constraints) for more.

```sh
+ git clone https://gitlab.com/efronlicht/build-tag-example
+ cd build-tag-example
+ go run tags=foo .
foo
+ go run .
bar
```
